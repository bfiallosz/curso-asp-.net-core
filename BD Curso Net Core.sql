-- BASE DE DATOS CURSO NET CORE --

CREATE TABLE [dbo].[tbl_TipoIdentificacion] (
    [idTipoIdentificacion] INT          IDENTITY (1, 1) NOT NULL,
    [nombre]               VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([idTipoIdentificacion] ASC)
);

CREATE TABLE [dbo].[tbl_Rol] (
    [idRol]       INT           IDENTITY (1, 1) NOT NULL,
    [nombre]      VARCHAR (50)  NOT NULL,
    [descripcion] VARCHAR (200) NOT NULL,
    [estado]      BIT           DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([idRol] ASC)
);

CREATE TABLE [dbo].[tbl_Usuario] (
    [idUsuario]  INT          IDENTITY (1, 1) NOT NULL,
    [nombre]     VARCHAR (20) NOT NULL,
    [contrasena] VARCHAR (10) NOT NULL,
    [idRol]      INT          NOT NULL,
    [estado]     BIT          DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Table] PRIMARY KEY CLUSTERED ([idUsuario] ASC),
    CONSTRAINT [FK_tbl_Usuario_tbl_Rol] FOREIGN KEY ([idRol]) REFERENCES [dbo].[tbl_Rol] ([idRol])
);

CREATE TABLE [dbo].[tbl_Pais] (
    [idPais] INT          IDENTITY (1, 1) NOT NULL,
    [nombre] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([idPais] ASC)
);

CREATE TABLE [dbo].[tbl_Provincia] (
    [idProvincia] INT        IDENTITY (1, 1) NOT NULL,
    [idPais]      INT        NOT NULL,
    [nombre]      NCHAR (10) NOT NULL,
    PRIMARY KEY CLUSTERED ([idProvincia] ASC),
    CONSTRAINT [FK_tbl_Provincia_tbl_Pais] FOREIGN KEY ([idPais]) REFERENCES [dbo].[tbl_Pais] ([idPais])
);

CREATE TABLE [dbo].[tbl_Ciudad] (
    [idCiudad]    INT          IDENTITY (1, 1) NOT NULL,
    [idProvincia] INT          NOT NULL,
    [nombre]      VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([idCiudad] ASC),
    CONSTRAINT [FK_tbl_Ciudad_tbl_Provincia] FOREIGN KEY ([idProvincia]) REFERENCES [dbo].[tbl_Provincia] ([idProvincia])
);

CREATE TABLE [dbo].[tbl_Persona] (
    [idpersona]            INT          IDENTITY (1, 1) NOT NULL,
    [idTipoIdentificacion] INT          DEFAULT ((1)) NOT NULL,
    [identificacion]       VARCHAR (13) NOT NULL,
    [nombres]              VARCHAR (50) NOT NULL,
    [apellidos]            VARCHAR (50) NOT NULL,
    [idPais]               INT          DEFAULT ((1)) NOT NULL,
    [idProvincia]          INT          DEFAULT ((1)) NOT NULL,
    [idCiudad]             INT          DEFAULT ((1)) NOT NULL,
    [idUsuario]            INT          NOT NULL,
    PRIMARY KEY CLUSTERED ([idpersona] ASC),
    UNIQUE NONCLUSTERED ([identificacion] ASC),
    UNIQUE NONCLUSTERED ([identificacion] ASC),
    CONSTRAINT [FK_tbl_Persona_tbl_Usuario] FOREIGN KEY ([idUsuario]) REFERENCES [dbo].[tbl_Usuario] ([idUsuario]),
    CONSTRAINT [FK_tbl_Persona_tbl_Ciudad] FOREIGN KEY ([idCiudad]) REFERENCES [dbo].[tbl_Ciudad] ([idCiudad]),
    CONSTRAINT [FK_tbl_Persona_tbl_TipoIdentificacion] FOREIGN KEY ([idTipoIdentificacion]) REFERENCES [dbo].[tbl_TipoIdentificacion] ([idTipoIdentificacion]),
    CONSTRAINT [FK_tbl_Persona_tbl_Pais] FOREIGN KEY ([idPais]) REFERENCES [dbo].[tbl_Pais] ([idPais]),
    CONSTRAINT [FK_tbl_Persona_tbl_Provincia] FOREIGN KEY ([idProvincia]) REFERENCES [dbo].[tbl_Provincia] ([idProvincia])
);

-- INSERT EN LAS TABLAS

insert into tbl_TipoIdentificacion(nombre) values ('Cédula')
insert into tbl_TipoIdentificacion(nombre) values ('RUC')
insert into tbl_TipoIdentificacion(nombre) values ('Pasaporte')

insert into tbl_Pais(nombre) values('Ecuador')

insert into tbl_Provincia(idPais,nombre) values(1, 'Guayas')
insert into tbl_Provincia(idPais,nombre) values(1, 'Pichincha')

insert into tbl_Ciudad(idProvincia,nombre) values(1, 'Guayaquil')
insert into tbl_Ciudad(idProvincia,nombre) values(2, 'Quito')

insert into tbl_Rol(nombre, descripcion, estado) values('admin', 'Para administradores', 1)
insert into tbl_Rol(nombre, descripcion, estado) values('gestor', 'Para gestores', 1)

insert into tbl_Usuario(nombre,contrasena,idRol,estado) values('bfiallos','123456',1,1)
insert into tbl_Usuario(nombre,contrasena,idRol,estado) values('mtorres','123456',2,1)




